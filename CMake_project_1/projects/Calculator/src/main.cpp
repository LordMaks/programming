#include "iostream"
#include "Calculator.h"
#include "conio.h"

using namespace std;

int main()
{
  
  cout << "Hello from Calc." << endl;

  auto a = -11.2, b = -12.93, x = 2.29;

  auto result = calc::calculate(x,a,b);
  cout << "Result 1: " << result << endl;

  if (calc::calculate(x,a,b,result))
  {
      cout << "Result 2: " << result << endl;
  }

  calc::Calculator calculator(a,b);
  cout << "Calculator 1: " << calculator.calculate(x) << endl;
  getch();
  return 0;

}
